package com.example.github.services.managers

import com.example.github.services.RequestCallback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

abstract class BaseManager {

    protected val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_GITHUB_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()


    protected fun <T> processData(response: retrofit2.Response<T>?, callback: RequestCallback<T>) {
        if (response?.body() == null) {
            callback.onError(Exception())

        } else if (response.body() != null && response.body() != null) {
            callback.onSuccess(response.body()!!)
        } else {
            callback.onError(Exception())
        }
    }

    protected fun <T, U> processDataWithAction(
        response: retrofit2.Response<T>?,
        callback: RequestCallback<U>?,
        processing: AdditionalProcessing<T, U>
    ) {

        if (response?.body() == null) {
            callback?.onError(Exception())
        } else if (response.body() != null && response.body() != null && response.body() != null) {
            callback?.onSuccess(processing.action(response.body()!!))
        } else {
            callback?.onError(Exception())
        }
    }

    protected fun <T> processError(throwable: Throwable, callback: RequestCallback<T>) {
        callback.onError(throwable)
    }

    interface AdditionalProcessing<T, U> {
        fun action(parameter: T): U
    }

    companion object {
        const val BASE_GITHUB_URL = "https://api.github.com"
    }
}