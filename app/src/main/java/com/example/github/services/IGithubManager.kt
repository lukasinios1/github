package com.example.github.services

import com.example.github.models.User
import com.example.github.models.UserDetail

interface IGithubManager {

    fun getAllGithubUsers(requestCallback: RequestCallback<List<User>>)

    fun getUserDetails(userName: String, requestCallback: RequestCallback<UserDetail>)

    fun getUsersFromDatabase(requestCallback: RequestCallback<List<User>>)
}