package com.example.github.services.managers

import com.example.github.database.UsersDao
import com.example.github.models.User
import com.example.github.models.UserDetail
import com.example.github.services.IGithubManager
import com.example.github.services.RequestCallback
import com.example.github.services.api.GithubApi
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GithubManager(private val userDao: UsersDao) : BaseManager(), IGithubManager {

    private val githubService = retrofit.create(GithubApi::class.java)

    override fun getAllGithubUsers(requestCallback: RequestCallback<List<User>>) {
        githubService.getAllGithubUsers().enqueue(object : Callback<List<User>> {
            override fun onFailure(call: Call<List<User>>, throwable: Throwable) {
                processError(throwable, requestCallback)
            }

            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                processDataWithAction(
                    response,
                    requestCallback,
                    object : AdditionalProcessing<List<User>, List<User>> {
                        override fun action(parameter: List<User>): List<User> {
                            doAsync {
                                userDao.insertAllUsers(parameter)
                            }
                            return parameter
                        }
                    })
            }
        })
    }

    override fun getUserDetails(userName: String, requestCallback: RequestCallback<UserDetail>) {
        githubService.getUserDetail(userName).enqueue(object : Callback<UserDetail> {
            override fun onFailure(call: Call<UserDetail>, throwable: Throwable) {
                processError(throwable, requestCallback)
            }

            override fun onResponse(call: Call<UserDetail>, response: Response<UserDetail>) {
                processData(response, requestCallback)
            }

        })
    }

    override fun getUsersFromDatabase(requestCallback: RequestCallback<List<User>>) {
        doAsync {
            val usersFromDao = userDao.allUsers
            if (usersFromDao.isNotEmpty()) {
                requestCallback.onSuccess(usersFromDao)
            } else {
                requestCallback.onError(Throwable())
            }
        }
    }

}