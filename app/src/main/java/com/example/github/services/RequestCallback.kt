package com.example.github.services

interface RequestCallback<T> {

    fun onSuccess(response: T)

    fun onError(error: Throwable)
}