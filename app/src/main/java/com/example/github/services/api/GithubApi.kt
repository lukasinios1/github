package com.example.github.services.api

import com.example.github.models.User
import com.example.github.models.UserDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubApi {

    @GET("/users")
    fun getAllGithubUsers(): Call<List<User>>

    @GET("/users/{userName}")
    fun getUserDetail(@Path("userName") userName: String): Call<UserDetail>
}