package com.example.github.base

import android.arch.lifecycle.Lifecycle
import android.os.Bundle
import com.example.github.database.AppDatabase


interface BaseContract {

    interface View

    interface Presenter<V : BaseContract.View> {
        fun attachLifecycle(lifecycle: Lifecycle)

        fun detachLifecycle(lifecycle: Lifecycle)

        fun attachView(view: V)

        fun detachView()

        fun getView(): V?

        fun isViewAttached(): Boolean

        fun onPresenterDestroy()

        fun getStateBundle(): Bundle

        fun isTablet(isTablet: Boolean)

        fun setGithubManager(appDatabase: AppDatabase)
    }
}