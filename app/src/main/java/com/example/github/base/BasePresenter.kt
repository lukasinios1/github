package com.example.github.base

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.os.Bundle
import com.example.github.database.AppDatabase
import com.example.github.services.IGithubManager
import com.example.github.services.managers.GithubManager

abstract class BasePresenter<V : BaseContract.View> : LifecycleObserver, BaseContract.Presenter<V> {

    private var view: V? = null
    private var stateBundle: Bundle? = null
    protected lateinit var githubManger: IGithubManager
    protected var isTablet: Boolean = false

    override fun getView(): V? = view

    override fun attachLifecycle(lifecycle: Lifecycle) {
        lifecycle.addObserver(this)
    }

    override fun detachLifecycle(lifecycle: Lifecycle) {
        lifecycle.removeObserver(this)
    }

    override fun attachView(view: V) {
        this.view = view
    }

    override fun setGithubManager(appDatabase: AppDatabase) {
        githubManger = GithubManager(appDatabase.usersDao())
    }

    override fun detachView() {
        view = null
    }

    override fun isViewAttached(): Boolean = view != null

    override fun getStateBundle(): Bundle = if (stateBundle == null) Bundle() else stateBundle!!

    override fun onPresenterDestroy() {
        if (stateBundle != null && !stateBundle!!.isEmpty) {
            stateBundle!!.clear()
        }
    }

    override fun isTablet(isTablet: Boolean) {
        this.isTablet = isTablet
    }
}