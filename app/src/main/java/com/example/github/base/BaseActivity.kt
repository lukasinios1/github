package com.example.github.base

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.github.R
import com.example.github.database.AppDatabase


abstract class BaseActivity<V : BaseContract.View, P : BaseContract.Presenter<V>> : AppCompatActivity(),
    BaseContract.View {

    private val lifecycleRegistry = LifecycleRegistry(this)
    protected var presenter: P? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel: BaseViewModel<V, P> = ViewModelProviders.of(this).get(BaseViewModel<V, P>()::class.java)
        if (viewModel.presenter == null) {
            viewModel.presenter = initPresenter()
        }
        presenter = viewModel.presenter
        presenter?.setGithubManager(AppDatabase.getAppDataBase(this))
        presenter?.attachLifecycle(lifecycle)
        presenter?.attachView(this as V)
        presenter?.isTablet(resources.getBoolean(R.bool.isTablet))
    }

    override fun getLifecycle(): Lifecycle = lifecycleRegistry

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachLifecycle(lifecycle)
        presenter?.detachView()
    }

    protected abstract fun initPresenter(): P

}