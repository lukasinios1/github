package com.example.github.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserDetail(
    @SerializedName("avatar_url")
    val avatarUrl: String?,
    @SerializedName("bio")
    val bio: String?,
    @SerializedName("blog")
    val blog: String?,
    @SerializedName("company")
    val company: String?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("events_url")
    val eventsUrl: String?,
    @SerializedName("followers")
    val followers: Int?,
    @SerializedName("followers_url")
    val followersUrl: String?,
    @SerializedName("following")
    val following: Int?,
    @SerializedName("following_url")
    val followingUrl: String?,
    @SerializedName("gists_url")
    val gistsUrl: String?,
    @SerializedName("gravatar_id")
    val gravatarId: String?,
    @SerializedName("hireable")
    val hireable: String?,
    @SerializedName("html_url")
    val htmlUrl: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("location")
    val location: String?,
    @SerializedName("login")
    val login: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("node_id")
    val nodeId: String?,
    @SerializedName("organizations_url")
    val organizationsUrl: String?,
    @SerializedName("public_gists")
    val publicGists: Int?,
    @SerializedName("public_repos")
    val publicRepos: Int?,
    @SerializedName("received_events_url")
    val receivedEventsUrl: String?,
    @SerializedName("repos_url")
    val reposUrl: String?,
    @SerializedName("site_admin")
    val siteAdmin: Boolean?,
    @SerializedName("starred_url")
    val starredUrl: String?,
    @SerializedName("subscriptions_url")
    val subscriptionsUrl: String?,
    @SerializedName("type")
    val type: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("url")
    val url: String?
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readValue(Int::class.java.classLoader) as Int?,
        source.readString(),
        source.readValue(Int::class.java.classLoader) as Int?,
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readValue(Int::class.java.classLoader) as Int?,
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readValue(Int::class.java.classLoader) as Int?,
        source.readValue(Int::class.java.classLoader) as Int?,
        source.readString(),
        source.readString(),
        source.readValue(Boolean::class.java.classLoader) as Boolean?,
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(avatarUrl)
        writeString(bio)
        writeString(blog)
        writeString(company)
        writeString(createdAt)
        writeString(email)
        writeString(eventsUrl)
        writeValue(followers)
        writeString(followersUrl)
        writeValue(following)
        writeString(followingUrl)
        writeString(gistsUrl)
        writeString(gravatarId)
        writeString(hireable)
        writeString(htmlUrl)
        writeValue(id)
        writeString(location)
        writeString(login)
        writeString(name)
        writeString(nodeId)
        writeString(organizationsUrl)
        writeValue(publicGists)
        writeValue(publicRepos)
        writeString(receivedEventsUrl)
        writeString(reposUrl)
        writeValue(siteAdmin)
        writeString(starredUrl)
        writeString(subscriptionsUrl)
        writeString(type)
        writeString(updatedAt)
        writeString(url)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<UserDetail> = object : Parcelable.Creator<UserDetail> {
            override fun createFromParcel(source: Parcel): UserDetail = UserDetail(source)
            override fun newArray(size: Int): Array<UserDetail?> = arrayOfNulls(size)
        }
    }
}