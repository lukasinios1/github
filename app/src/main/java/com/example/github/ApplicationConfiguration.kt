package com.example.github

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager

class ApplicationConfiguration : Application() {

    companion object {
        lateinit var instance: ApplicationConfiguration private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    fun isConnection(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null
    }
}