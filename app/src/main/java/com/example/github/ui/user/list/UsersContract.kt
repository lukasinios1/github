package com.example.github.ui.user.list

import com.example.github.base.BaseContract
import com.example.github.models.User
import com.example.github.models.UserDetail

interface UsersContract {

    interface View : BaseContract.View {
        fun showProgress()

        fun hideProgress()

        fun onGetGithubUsersSuccess(githubUsers: List<User>)

        fun onGetGithubUsersFailed()

        fun onGetUserDetailsSuccessTablet(userDetail: UserDetail)

        fun onGetUserDetailsSuccess(userDetail: UserDetail)

        fun onGetUserDetailsFailed()

        fun onGetUserDetailsFailedTablet()

        fun updateUsers(updatedUsers: List<User>)
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun getGithubUsers()

        fun getGithubUserDetails(userName: String)

        fun filterItems(query: String)

        fun setUserDetailsAfterScreenOrientationChange()
    }
}