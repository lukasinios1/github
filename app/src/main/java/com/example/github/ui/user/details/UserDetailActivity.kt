package com.example.github.ui.user.details

import android.os.Bundle
import com.example.github.R
import com.example.github.base.BaseActivity
import com.example.github.models.UserDetail
import com.example.github.ui.user.list.UsersActivity.Companion.USER_DETAIL
import kotlinx.android.synthetic.main.item_user_details.*

class UserDetailActivity : BaseActivity<UserDetailContract.View, UserDetailContract.Presenter>(),
    UserDetailContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_github_user_detail)

        val userDetail: UserDetail? = intent?.extras?.get(USER_DETAIL) as UserDetail?

        company_content?.text = userDetail?.company
        email_content?.text = userDetail?.email
        login_content?.text = userDetail?.login
    }

    override fun initPresenter(): UserDetailContract.Presenter = UserDetailPresenter()
}