package com.example.github.ui.user.list

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.github.R
import com.example.github.models.User
import kotlinx.android.synthetic.main.item_user.view.*

class UsersAdapter(private val userCallback: UserCallback) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    private var usersList: MutableList<User> = mutableListOf()

    fun setUsers(usersList: MutableList<User>) {
        this.usersList.clear()
        this.usersList = usersList
        notifyDataSetChanged()
    }

    fun updateUsers(updatedUsers: List<User>) {
        val diffCallback = UsersDiffUtillCallback(usersList, updatedUsers)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.usersList.clear()
        this.usersList.addAll(updatedUsers)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false))

    override fun getItemCount(): Int = usersList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(usersList[position], userCallback)

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(user: User, userCallback: UserCallback) {
            Glide.with(view)
                .load(user.avatarUrl)
                .into(view.user_image)

            view.user_login.text = user.login

            view.setOnClickListener {
                userCallback.onUserClickCallback(user)
            }

        }
    }

    interface UserCallback {
        fun onUserClickCallback(user: User)
    }
}