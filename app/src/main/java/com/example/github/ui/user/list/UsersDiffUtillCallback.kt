package com.example.github.ui.user.list

import android.support.v7.util.DiffUtil
import com.example.github.models.User

class UsersDiffUtillCallback(private val oldUsersList: List<User>, private val newUsersList: List<User>) :
    DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldUsersList[oldItemPosition].id == newUsersList[newItemPosition].id

    override fun getOldListSize(): Int = oldUsersList.size

    override fun getNewListSize(): Int = newUsersList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldUsersList[oldItemPosition].login == newUsersList[newItemPosition].login

}