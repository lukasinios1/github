package com.example.github.ui.user.details

import com.example.github.base.BasePresenter

class UserDetailPresenter : BasePresenter<UserDetailContract.View>(), UserDetailContract.Presenter {
}