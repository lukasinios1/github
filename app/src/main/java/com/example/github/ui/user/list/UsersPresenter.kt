package com.example.github.ui.user.list

import com.example.github.ApplicationConfiguration
import com.example.github.base.BasePresenter
import com.example.github.models.User
import com.example.github.models.UserDetail
import com.example.github.services.RequestCallback
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class UsersPresenter : BasePresenter<UsersContract.View>(), UsersContract.Presenter {

    private val usersList: MutableList<User> = mutableListOf()

    override fun getGithubUsers() {
        getView()?.showProgress()
        if (ApplicationConfiguration.instance.isConnection()) {
            githubManger.getAllGithubUsers(getUsersRequestCallback())
        } else {
            githubManger.getUsersFromDatabase(getUsersRequestCallback())
        }
    }

    private fun getUsersRequestCallback() = object : RequestCallback<List<User>> {
        override fun onSuccess(response: List<User>) {
            usersList.clear()
            usersList.addAll(response.requireNoNulls())

            getView()?.hideProgress()
            getView()?.onGetGithubUsersSuccess(response)
            if (isTablet && response.isNotEmpty()) {
                response[0].login?.let { userLogin -> getGithubUserDetails(userLogin) }
                    ?: kotlin.run { getView()?.onGetUserDetailsFailedTablet() }
            }
        }

        override fun onError(error: Throwable) {
            getView()?.hideProgress()
            getView()?.onGetGithubUsersFailed()
        }
    }

    override fun setUserDetailsAfterScreenOrientationChange() {
        if (isTablet) {
            if (usersList.isNotEmpty() && usersList[0].login != null) {
                getGithubUserDetails(usersList[0].login!!)
            } else {
                getView()?.onGetUserDetailsFailedTablet()
            }
        }
    }


    override fun getGithubUserDetails(userName: String) {
        githubManger.getUserDetails(userName, object : RequestCallback<UserDetail> {
            override fun onSuccess(response: UserDetail) {
                if (isTablet) {
                    getView()?.onGetUserDetailsSuccessTablet(response)
                } else {
                    getView()?.onGetUserDetailsSuccess(response)
                }
            }

            override fun onError(error: Throwable) {
                if (isTablet) {
                    getView()?.onGetUserDetailsFailedTablet()
                } else {
                    getView()?.onGetUserDetailsFailed()
                }

            }

        })
    }

    override fun filterItems(query: String) {
        doAsync {
            val filteredList = usersList.asSequence().filter {
                it.login?.contains(query, true) ?: false
            }.toList()

            uiThread {
                getView()?.updateUsers(filteredList)
            }
        }
    }

}