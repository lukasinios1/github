package com.example.github.ui.user.details

import com.example.github.base.BaseContract

interface UserDetailContract {

    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter<UserDetailContract.View>
}