package com.example.github.ui.user.list

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import com.example.github.R
import com.example.github.base.BaseActivity
import com.example.github.models.User
import com.example.github.models.UserDetail
import com.example.github.ui.user.details.UserDetailActivity
import kotlinx.android.synthetic.main.activity_github.*
import kotlinx.android.synthetic.main.item_user_details.*

class UsersActivity : BaseActivity<UsersContract.View, UsersContract.Presenter>(), UsersContract.View,
    UsersAdapter.UserCallback {

    private lateinit var adapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_github)
        setAdapter()
        setSearchView()

        presenter?.getGithubUsers()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        if (newConfig?.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            presenter?.setUserDetailsAfterScreenOrientationChange()
        }
    }

    private fun setAdapter() {
        adapter = UsersAdapter(this)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = adapter
    }

    private fun setSearchView() {

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                presenter?.filterItems(query.orEmpty())
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                presenter?.filterItems(query.orEmpty())
                return false
            }
        })
    }

    override fun initPresenter(): UsersContract.Presenter = UsersPresenter()

    override fun showProgress() {
        //todo showProgress
    }

    override fun hideProgress() {
        //todo hideProgress
    }

    override fun onGetGithubUsersSuccess(githubUsers: List<User>) {
        adapter.setUsers(githubUsers.toMutableList())
    }

    override fun onGetGithubUsersFailed() {
        //todo showErrorMask
    }

    override fun onUserClickCallback(user: User) {
        presenter?.getGithubUserDetails(user.login ?: "")
    }

    override fun onGetUserDetailsSuccessTablet(userDetail: UserDetail) {
        company_content?.text = userDetail.company
        email_content?.text = userDetail.email
        login_content?.text = userDetail.login
    }

    override fun onGetUserDetailsSuccess(userDetail: UserDetail) {
        val intent = Intent(this, UserDetailActivity::class.java)
        intent.putExtra(USER_DETAIL, userDetail)
        startActivity(intent)
    }

    override fun onGetUserDetailsFailed() {
        //todo showErrorMask
    }

    override fun onGetUserDetailsFailedTablet() {
        //todo showErrorMask
    }

    override fun updateUsers(updatedUsers: List<User>) {
        adapter.updateUsers(updatedUsers)
    }

    companion object {
        const val USER_DETAIL = "USER_DETAIL"
    }
}